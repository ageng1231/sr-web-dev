
# Sr. Web Dev - Ageng Muhammad Wijayanto

A brief description of what this project does and who it's for


## Authors

- [@ageng1231](https://www.gitlab.com/ageng1231)


## Deployment

To deploy this project run
1. Install Composer Dependencies
```bash
  composer install
```
2. Install NPM Dependencies
```bash
  yarn add
```
3. create .env file
4. fill database credentials, host and port
5. run migration

```bash
  php artisan migrate
```
5. seed database

```bash
  php artisan db:seed
```
6. serve laravel

```bash
  php artisan serve --host = 0.0.0.0 --port = 8081
```
## Credentials
All User Password with db:seed will be 
```bash
    password = 'password'
```
## Licenses

Add badges from somewhere like: [shields.io](https://shields.io/)

[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://choosealicense.com/licenses/mit/)
[![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-yellow.svg)](https://opensource.org/licenses/)
[![AGPL License](https://img.shields.io/badge/license-AGPL-blue.svg)](http://www.gnu.org/licenses/agpl-3.0)

## Tech Stack

![MySQL](https://img.shields.io/badge/mysql-4479A1.svg?style=for-the-badge&logo=mysql&logoColor=white)
![Laravel](https://img.shields.io/badge/laravel-%23FF2D20.svg?style=for-the-badge&logo=laravel&logoColor=white)
![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)
![React Hook Form](https://img.shields.io/badge/React%20Hook%20Form-%23EC5990.svg?style=for-the-badge&logo=reacthookform&logoColor=white)
![Redux](https://img.shields.io/badge/redux-%23593d88.svg?style=for-the-badge&logo=redux&logoColor=white)
![Vite](https://img.shields.io/badge/vite-%23646CFF.svg?style=for-the-badge&logo=vite&logoColor=white)
![Ant-Design](https://img.shields.io/badge/-AntDesign-%230170FE?style=for-the-badge&logo=ant-design&logoColor=white)
![MUI](https://img.shields.io/badge/MUI-%230081CB.svg?style=for-the-badge&logo=mui&logoColor=white)
