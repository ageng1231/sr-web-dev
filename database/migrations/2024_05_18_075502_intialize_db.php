<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::create("transaction", function(Blueprint $table){
            $table->id();
            $table->unsignedBigInteger("user_id");
            $table->string("penerima")->nullable();
            $table->string("tujuan")->nullable();
            $table->string("kode_transaksi",50)->unique();
            $table->string("type_trx",191);
            $table->unsignedBigInteger("amount");
            $table->text("keterangan")->nullable();
            $table->string("status");
            $table->string("file")->nullable();
            $table->timestamps();
        });
        Schema::create("saldo", function(Blueprint $table){
            $table->id();
            $table->unsignedBigInteger("user_id");
            $table->unsignedBigInteger("saldo")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
