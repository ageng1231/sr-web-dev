<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Saldos;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'root',
        //     'email' => 'admin',
        // ]);
        $user = User::all();
        foreach($user as $row){
            Saldos::factory()->create([
                'user_id' => $row->id,
                "saldo" => 0
            ]);
        }
    }
}
