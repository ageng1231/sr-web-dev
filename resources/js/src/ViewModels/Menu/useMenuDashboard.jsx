import React from "react";
import { staticLogInMenu } from "../../Models/menu-models";

export const useMenuDashboard = () => {
    const [listMenu, setListMenu] = React.useState(staticLogInMenu)
    const onClick = (e) => {
        switch(e.key){
            case "topup":
                window.location.href = "/transaksi/topup"
            break;
            case "dashboard":
                window.location.href = "/dashboard"
            break;
            case "history":
                window.location.href = "/history"
            break;
        }
    }
    return {
        listMenu,
        onClick
    }
}
export default useMenuDashboard;
