import React from "react"
import { staticLogInMenu } from "../../Models/menu-models"
import { initializeDb } from "../../Models/LocalStorage/db";

export const useDashboardHome = () => {
    initializeDb();
}
export default useDashboardHome
