import React from "react";
import { fetchHistoryAll, findHistory, queryHistoryAll } from "../../Models/Api/HistoryApiModels";
import { message } from "antd";

export const useHistoryPage = () => {
    const selectedMenu = "history"
    const [dataSource, setDataSource] = React.useState([

    ])
    const [modalOpen, setModalOpen] = React.useState(false)
    const [columns, setColumns] = React.useState([
        {
            title: "Nomor Transaksi",
            dataIndex: "kode_transaksi",
            key: "kode_transaksi",
        },

        {
            title: "Tanggal Transaksi",
            dataIndex: "updated_at",
            key: "updated_at",
        },
        {
            title: "Nominal",
            dataIndex: "amount",
            key: "amount",
        },
        {
            title: "Tipe Transaksi",
            dataIndex: "type_trx",
            key: "type_trx",
        },
        {
            title: "Keterangan",
            dataIndex: "keterangan",
            key: "keterangan",
        },
        {
            title: "Status",
            dataIndex: "status",
            key: "status-trx"
        }
    ])
    const [filter, setFilter] = React.useState({
        status: "",
        amount: "",
    })
    const [search, setSearch] = React.useState("")
    const [filterList, setFilterList] = React.useState()
    const [messageApi, contextHolder] = message.useMessage()
    const [modalData, setModalData] = React.useState({
        updated_at: "",
        type_trx: "",
        kode_transaksi: "",
        amount: "",
        bukti_transaksi: "",
        status: "",
    })
    const [currentPage, setCurrentPage] = React.useState(1)
    const loadData = async () => {
        let data = await fetchHistoryAll().then(r => r.data).catch(err => err.response.data);
        if(data?.success == undefined || data?.success == false){
            messageApi.error("Terjadi Kesalahan Saat Melakukan Pengambilan Data")
        }

        setDataSource(data.data);
    }
    const openModal = async (val) => {
        // setModalData();
        await findHistory(val)
        .then(r => {

            setModalData(r.data.data)
        })
        .catch(err => {

        })
        setModalOpen(true);

    }
    const searchData = async () => {
        let params = {
            q: search,
            filter: filter
        }
        let data = await queryHistoryAll(params).then(r => r.data.data).catch(err => err.response.data);
        setDataSource(data)

    }
    const searchHandler = (val) => {
        setSearch(val)
    }
    const changeSelectedFilter = (val) => {
        setFilter(val)
    }
    const removeFilterStatus = () => {
        setFilter({...filter, status: ""})
    }
    const removeFilterAmount = () => {
        setFilter({...filter, amount: ""})
    }
    React.useEffect(() => {
        const debouncing = setTimeout(() => {
            if(search != "" || filter.status != "" || filter.amount != ""){
                searchData()
            }else{
                loadData()
            }

        }, 1000);
        return () => clearTimeout(debouncing)
    }, [search, filter])
    const modalClose = () => {
        // loadData();
        setModalOpen(false)
    }
    const paginationHandler = (val) => {
        setCurrentPage(val);
    }
    return {
        selectedMenu,
        dataSource,
        filter,
        search,
        filterList,
        columns,
        contextHolder,
        modalOpen,
        modalData,
        openModal,
        modalClose,
        searchHandler,
        currentPage,
        paginationHandler,
        changeSelectedFilter,
        removeFilterStatus,
        removeFilterAmount
    }
}
export default useHistoryPage;
