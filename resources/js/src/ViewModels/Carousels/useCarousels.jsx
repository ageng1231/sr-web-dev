import React from "react";
import carouselModel from "../../Models/Carousels/carousel-model";

export const useCarousel = () => {
    const [imageList, setImageList] = React.useState([]);
    const loadData = async () => {
        let model = new carouselModel();
        let data  = await model.loadStatic()
        setImageList(data)
        setInterval(() => {
            let max = data.length;
            let after = setAutoPlay(autoPlay + 1);
            if(after > max){
                after = 0;
            }
            setAutoPlay(after)
        },10)
    }
    const [autoPlay, setAutoPlay] = React.useState(0)
    React.useEffect(() => {
        loadData();
        return () => {}
    },[])
    return {
        imageList,
        autoPlay
    }
}
export default useCarousel;
