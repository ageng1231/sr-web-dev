import {Form, message} from "antd";
import { submitTransferForm } from "../../Models/Api/TransaksciApiModels";
import React from "react";
import { usePage } from "@inertiajs/inertia-react";
export const useTopUpPage = () => {
    const selectedMenu = "topup";
    const [form] = Form.useForm();
    const [messageApi, contextHolder] = message.useMessage();
    const [uploadedFile, setUploadedFile] = React.useState([]);
    const submitHandler = async (val) => {
        messageApi.loading("Mengirim Form Transaksi...")
        submitTransferForm(val).then(r => {
            console.warn(r)
            messageApi.success("Berhasil Mengirim Form Transaksi... ")
            window.location.reload()
        })
        .catch(err => {
            console.warn(err)
        })
    }
    const page = usePage().props
    const [previewImg, setPreviewImg] = React.useState("")
    const [previewOpen, setpreviewOpen] = React.useState(false)

    const [uploader, setUploader] = React.useState({
        name: "bukti_trx",
        multiple: false,
        action: "http://localhost:8000/file/upload/bukti_trx",
        accept: "image/*",
        listType: "picture-card",
        onPreview: (file) => {
            handlePreview(file)
        },
        onChange: (val) => {
            console.warn(val)
            setUploadedFile(val.fileList)
            console.warn(val.fileList)
            form.setFieldValue("bukti_transaksi", val.file.response?.data?.name ?? "")
            return
        },

        onDrop: (fileInfo) => {
            console.warn(fileInfo)
        },
    })
    const getBase64 = (file) =>
        new Promise((resolve, reject) => {
          const reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = () => resolve(reader.result);
          reader.onerror = (error) => reject(error);
    });
    const handlePreview = async (file) => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
          }
          setPreviewImg(file.url || file.preview);
          setpreviewOpen(true);
    }
    const invalidFormHandler = async () => {
        messageApi.error("Gagal Mengirim Form Harap Periksa Kembali Form Transaksi Anda ... ")
    }
    const onVisibleHandler = (visible) => {
        setpreviewOpen(visible)
    }
    const onPreviewImgCloseHandler = () => {
        setPreviewImg("")
    }
    return {
        form,
        selectedMenu,
        contextHolder,
        submitHandler,
        invalidFormHandler,
        uploader,
        previewOpen,
        previewImg,
        onVisibleHandler,
        onPreviewImgCloseHandler
    }
}
export default useTopUpPage;
