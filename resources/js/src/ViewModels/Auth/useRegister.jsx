import { Form, message } from "antd"
import React from "react"

export const useRegister = () => {
    const [form] = Form.useForm()
    const [messageApi, contextHolder] = message.useMessage()
    const [refresh, setRefresh] = React.useState(false)
    const validForm = (val) => {
        setRefresh(true)
        messageApi.loading("Form Valid. Submit Data to Server ...");
        setTimeout(() => {
            messageApi.success("Berhasil Melakukan Registerasi Data")
            setRefresh(false)
            window.location.href = "/dashboard"
        }, 2000)
    }
    const invalidForm = (err) => {
        messageApi.error("Gagal Submit Data. Harap Cek Kembali Form Registerasi Anda")
    }

    return {
        form,
        messageApi,
        contextHolder,
        validForm,
        invalidForm,
        refresh
    }
}
