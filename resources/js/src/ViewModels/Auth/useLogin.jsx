import { Form, message } from "antd";
import axios from "axios";
import React from "react";

export const useLogin = (props) => {
    const [form] = Form.useForm();
    const [refresh, setRefreshing] = React.useState(false);
    const [messageApi, contextHolder] = message.useMessage();
    const loginHandler = async (val) => {
        messageApi.loading("Melakukan Pengecekan Data")
        setRefreshing(true)
        axios.post(props.app.baseUri + "/auth/signin", val)

        .then((res) => {
            messageApi.success("Berhasil Melakukan Authentikasi Data")
            messageApi.loading("Melakukan Redirect Ke Halaman Dashboard")
            window.location.href = props.app.baseUri + "/dashboard"
        })
        .catch((err) => {
            console.warn(err);
            messageApi.error(err.response.data.message)
            setRefreshing(false)
        })

    }
    return {
        form,
        contextHolder,
        loginHandler,
        refresh
    }
}
export default useLogin;
