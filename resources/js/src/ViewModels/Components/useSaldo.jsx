import React from "react"
import SaldoApiModels from "../../Models/Api/SaldoApiModels"
import { loadSaldo } from "../../Domains/Api/saldo";

export const useSaldo = () => {
    const [saldo, setSaldo] = React.useState({
        balance: 0,
        total_trx: 0,
        month_trx: 0
    })
    const apiModels = SaldoApiModels;
    const loadData = async () => {
        let data = await loadSaldo().then(r => r.data).catch(err => err.response.data)
        setSaldo(data)
    }
    React.useEffect(() => {
        loadData();
        return () => {

        }
    }, [])
    return {
        saldo
    }
}
export default useSaldo
