import React from "react"

export const useFilterModal = () => {
    const [modalOpen, setModalOpen] = React.useState(false)
    const [title, setTitle] = React.useState("Filter Data ")
    const [filterSelected, setFilterSelected] = React.useState({
        status: "",
        amount: "",
    })
    const closeModal = () => {
        setModalOpen(false)
    }
    const openModal = () => {
        setModalOpen(true)
        console.warn(modalOpen)
    }
    const setModalTitle = (val) => {
        setTitle(val)
    }
    const selectStatusFilter = (val) => {
        if(filterSelected.status == val){
            setFilterSelected({...filterSelected, status: ""})
        }else{
            setFilterSelected({...filterSelected, status: val})
        }

    }
    const selectByAmount = (val) => {
        if(val == filterSelected.amount){
            setFilterSelected({...filterSelected, amount: ""})
        }else{
            setFilterSelected({...filterSelected, amount: val})
        }

    }
    return {
        modalOpen,
        closeModal,
        openModal,
        title,
        setModalTitle,
        selectStatusFilter,
        filterSelected,
        selectByAmount
    }
}
export default useFilterModal
