import React from "react";
import { MenuList, RightMenu } from "../Models/menu-models";

export const useNavigationBar = () => {
    const [menu, setMenu] = React.useState([])
    const [rightMenu, setRightMenu] = React.useState([])
    const [isLoggedIn, setIsLoggedIn] = React.useState(false)
    const [openDrawer, setOpenDrawer] = React.useState(false)
    const toggleDrawer = () => {
        setOpenDrawer(!openDrawer);
    }
    React.useEffect(() => {
        setMenu(MenuList);
        setRightMenu(RightMenu)
        return () => {

        }
    })
    const menuClick = (e) => {
        console.warn(e)
        switch(e.key){
            case "logout":
                window.location.href = "/logout"
            break;
        }
    }
    return {
        menu,
        rightMenu,
        isLoggedIn,
        toggleDrawer,
        openDrawer,
        menuClick
    }
}
export default useNavigationBar;
