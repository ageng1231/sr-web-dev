import React from "react";

const idb =
  window.indexedDB ||
  window.mozIndexedDB ||
  window.webkitIndexedDB ||
  window.msIndexedDB ||
  window.shimIndexedDB;


  export const initializeDb = () => {
        if(!idb){
            console.warn("Your Browser does not support our PWA services")
            return
        }
        const request = idb.open("ide-asia-db",1)
        const [error,setError] = React.useState(false)
        request.onerror = (event) => {
            setError(true)
        }
        request.onupgradeneeded = function (event) {
            console.log(event);
            const db = request.result;


            if (!db.objectStoreNames.contains("transaction")) {
                const objectStore = db.createObjectStore("transaction", { keyPath: "id" });
            }
            if (!db.objectStoreNames.contains("topup_target")) {
                const objectStore = db.createObjectStore("topup_target", { keyPath: "id" });
            }
            if (!db.objectStoreNames.contains("dashboard_data")) {
                const objectStore = db.createObjectStore("dashboard_data", { keyPath: "id" });
            }

        };
        return {
            idb,
            error
        }
  }
