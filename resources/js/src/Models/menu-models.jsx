import { AccountBookOutlined, DashboardOutlined, DotChartOutlined, HistoryOutlined, HomeOutlined, LogoutOutlined, PayCircleFilled, SettingOutlined, TransactionOutlined, UserOutlined, WalletOutlined } from "@ant-design/icons";
import Item from "antd/es/list/Item";
import { Children } from "react";

export const MenuList = [
    {
        label: "Home",
        key: "home",
        icon: <HomeOutlined />
    },
    {
        label: "Pricing",
        key: "pricing",
        icon: <WalletOutlined />
    },
    {
        label: "About Us",
        key: "about-us",
        icon: <AccountBookOutlined />
    }
];
export const RightMenu = [
    {
        label: "Account",
        key: "account",
        icon: <UserOutlined />,
        children: [
            {
                label: "Settings",
                key: "settings",
                icon: <SettingOutlined />
            },
            {
                label: "Log Out",
                key: "logout",
                icon: <LogoutOutlined  />,
                onTitleClicked: () => {
                    window.location.href = "/logout"
                }
            }
        ]
    }
]
export const staticLogInMenu = [
    {
        label: "Dashboard",
        key: "dashboard",
        icon: <DashboardOutlined />
    },
    {
        label: "Transaksi",
        key: "topup",
        icon: <TransactionOutlined />,

    },
    {
        label: "History",
        key: "history",
        icon: <HistoryOutlined />
    },
]
