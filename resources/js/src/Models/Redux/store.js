import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "./rootReducers";

const storeRedux = configureStore({
    reducer: rootReducer
})
