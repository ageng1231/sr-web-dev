import axios, { Axios } from "axios";

export const fetchHistoryAll = payload => axios.get("http://localhost:8000/history/all", payload);
export const queryHistoryAll = payload => axios.post("http://localhost:8000/history/all/q", payload);
export const findHistory = payload => axios.get("http://localhost:8000/history/find/" + payload,);

