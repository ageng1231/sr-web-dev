import React from "react";
import { loadSaldo } from "../../Domains/Api/saldo"

export const SaldoApiModels = () => {

    const loadSaldoData = async () => {
        return await loadSaldo()
    }

    return {
        loadSaldoData
    }
}
export default SaldoApiModels
