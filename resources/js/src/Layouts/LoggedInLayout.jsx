import NavbarMenu from "../Components/Navbar"
import NavigationLogIn from "../Components/NavigationLogIn"

export const LoggedInLayout = (props) => {
    return <div style={{height: "100%", width: "100%"}}>
        <NavigationLogIn />
        {props.children}
    </div>
}
export default LoggedInLayout
