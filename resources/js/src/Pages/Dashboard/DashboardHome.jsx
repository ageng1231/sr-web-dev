import { Breadcrumb, Card, Flex, Layout, Menu, Row, Col, Statistic, Button } from "antd";
import LoggedInLayout from "../../Layouts/LoggedInLayout";
import useDashboardHome from "../../ViewModels/Dashboard/useDashboardHome";
import MenuDashboard from "../../Components/Menu/MenuDashboard";
import { Container, LinearProgress } from "@mui/joy";
import Sider from "antd/es/layout/Sider";
import { Content } from "antd/es/layout/layout";
import { PlusCircleOutlined, PlusSquareTwoTone } from "@ant-design/icons";
import { StatisticSaldo } from "../../Components/Statistic/StatisticSaldo";

export const DashboardHome = () => {
    const vm = useDashboardHome()
    return <LoggedInLayout>
       <div style={{height: "100%", paddingLeft: 20, paddingTop: 12, paddingRight: 20}}>
       {/* <LinearProgress thickness={2} /> */}
            <MenuDashboard selected={"dashboard"}>
                <StatisticSaldo />
            </MenuDashboard>
       </div>
    </LoggedInLayout>
}
export default DashboardHome;
