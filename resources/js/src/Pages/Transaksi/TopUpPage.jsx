import {  Button, Flex, Form, Image, Select, Typography, Upload } from "antd"
import MenuDashboard from "../../Components/Menu/MenuDashboard"
import LoggedInLayout from "../../Layouts/LoggedInLayout"
import useTopUpPage from "../../ViewModels/Transaksi/useTopUpPage"
import TextField from "../../Components/Input/TextField"
import Dragger from "antd/es/upload/Dragger"
import { CloseOutlined, InboxOutlined, SendOutlined } from "@ant-design/icons"
import { LinearProgress } from "@mui/joy"
import TextAreaField from "../../Components/Input/TextAreaField"

export const TopupPage = () => {
    const vm = useTopUpPage();
        return <LoggedInLayout>
            {vm.contextHolder}
            <div style={{height: "100%", paddingLeft: 20, paddingTop: 12, paddingRight: 20}}>
            {/* <LinearProgress thickness={2} /> */}
                <MenuDashboard selected={vm.selectedMenu}>
                    <Form onFinish={(val) => vm.submitHandler(val)} onFinishFailed={() => vm.invalidFormHandler()} layout="vertical" form={vm.form}>
                        <Form.displayName>
                            <Typography.Title >
                                Transaksi
                            </Typography.Title>
                        </Form.displayName>
                        <Form.Item
                            label={"Tipe"}
                            name={"type"}
                            rules={[
                                {required: true, message: "Tipe Transaksi Wajib Diisi"}
                            ]}
                        >
                            <Select
                                placeholder="Pilih Tipe Transaksi"
                                options={[
                                    {value: "inbound_topup", label: "Top Up"},
                                    {value: "outbound_transaksi", label: "Transaksi"}
                                ]}
                            />
                        </Form.Item>
                        {/* <Form.Item
                            label={"Tujuan"}
                            name={"tujuan"}
                            rules={[
                                {required: true, message: "Tujuan Transaksi Wajib Diisi"}
                            ]}
                        >
                            <Select
                                placeholder="Pilih Tujuan Transaksi"
                                options={[
                                    {value: "topup", label: "Top Up"},
                                    {value: "transaksi", label: "Transaksi"}
                                ]}
                            />
                        </Form.Item> */}
                        {/* <Form.Item
                            label={"No Rek Tujuan"}
                            name={"no_rek_tujuan"}
                            rules={[
                                {required: true, message: "Nomor Rekening Wajib Diisi"}
                            ]}
                        >
                           <TextField />
                        </Form.Item> */}

                        <Form.Item
                        name={"amount"}
                        label={"Amount"}
                        rules={[
                            {required: true, message: "Jumlah Transaksi Wajib Diisi"}
                        ]}
                        >
                            <TextField />
                        </Form.Item>
                        <Form.Item
                        name={"keterangan"}
                        label={"Keterangan"}
                        rules={[

                        ]}
                        >
                            <TextAreaField />
                        </Form.Item>
                        <Form.Item  required={true}  valuePropName="bukti_trx" name={"bukti_transaksi"} dependencies={["bukti_transaksi"]} rules={[
                             {required: true, message: "Bukti Transaksi Harus Di Upload"}
                        ]} label={"Bukti Transaksi"}>
                            <Upload.Dragger
                                    {...vm.uploader}
                                >
                                    <p className="ant-aupload-drag-icon">
                                        <InboxOutlined />
                                    </p>
                                    <p className="ant-upload-text">Click Or Drag File into This Area to Upload</p>

                            </Upload.Dragger>
                            {vm.previewImg &&
                                <Image
                                    wrapperStyle={{
                                        display: 'none',
                                    }}
                                    preview={{
                                        visible: vm.previewOpen,
                                        onVisibleChange: (visible) => vm.onVisibleHandler(visible),
                                        afterOpenChange: (visible) => !visible && vm.onPreviewImgCloseHandler(''),
                                      }}
                                      src={vm.previewImg}

                                />
                            }
                        </Form.Item>

                        <Flex>
                            <div style={{flex: 1}}></div>
                            <Button style={{ width: 150, height: 50 }} type={"default"} size="large">
                                <CloseOutlined />
                                Cancel
                            </Button>
                            <div style={{width: 5}}></div>
                            <Button htmlType="submit" style={{ width: 150, height: 50 }} size={"large"} type={"primary"}>
                                <SendOutlined />
                                Kirim
                            </Button>
                        </Flex>

                    </Form>
                </MenuDashboard>
            </div>
        </LoggedInLayout>
}
export default TopupPage
