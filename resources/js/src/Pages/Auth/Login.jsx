import { UserOutlined } from "@ant-design/icons";
import { Button, Card, Flex, Form, Image, Input } from "antd";
import useLogin from "../../ViewModels/Auth/useLogin";
import SecureTextField from "../../Components/Input/SecureTextField"
import TextField from "../../Components/Input/TextField"
import { LinearProgress } from "@mui/joy";
export const Login = (props) => {
    const vm = useLogin(props);
    return <div style={{
        width: "100%",
        height: "100%"
    }}>
        {vm.contextHolder}
        {vm.refresh && <LinearProgress thickness={3} /> }
        <Flex style={{
            height: "100%"
        }}>
            <div style={{
                height: "100%",
                width: window.innerWidth < 1000 ? "0%" : "50%"
            }}>
            <Image
            wrapperStyle={{
                height: "100%"
            }}
            style={{
                height: "100%",
                objectFit: "cover"
            }} srcSet={"https://source.unsplash.com/random/1600x900?Payment"} />
            </div>


                <Flex style={{flexDirection: "column", height: "100%", width: window.innerWidth < 1000 ? "100%" : "50%"}}>
                  <div style={{flex: 1}}></div>
                  <Card style={{alignItems: "center"}}>
                  <Image onClick={() => {
                    window.location.href = "/dashboard"
                }} preview={false} style={{
                    padding: 12,
                    marginBottom: 50,
                    alignItems : "center",
                    alignContent: "center",
                    margin: "auto",
                    width: "100%",
                    objectFit: "cover",
                    cursor: "pointer"
                }} srcSet={"https://yukk.co.id/images/YUKK.png"} />
                  <Form
                    onFinish={(val) => vm.loginHandler(val)}
                    onFinishFailed={() => {
                        console.warn(this)
                    }}
                    form={vm.form} layout={"vertical"}>
                    <Form.Item

                        label={"Email or Username"}
                        name={"username"}
                        rules={[
                            {required: true, message:"Harap Isi Email / Username Terlebih Dahulu"}
                        ]}
                    >

                       <TextField />
                    </Form.Item>
                    <Form.Item dependencies={["password"]} name={"password"} label={"Password"} rules={[
                            {required: true, message:"Harap Isi Password Terlebih Dahulu"}
                        ]}>
                        <SecureTextField onChange={(value) => {

                            vm.form.setFieldValue("password", value.target.value)
                        }} />

                    </Form.Item>
                    <Flex align="center">
                            <div style={{flex: 1}}></div>
                            <p>Doesn't Have Account ?</p>

                            <Button onClick={() => {
                            window.location.href = "/register"
                        }}  type={"link"} style={{marginLeft: 2}}>
                                Sign Up Here
                            </Button>
                    </Flex>
                    <Form.Item>
                        <Button htmlType="submit" type={"primary"} style={{width: "100%"}}>
                            <UserOutlined />
                            Sign In
                        </Button>
                    </Form.Item>
                  </Form>
                  </Card>
                  <div style={{flex: 1}}></div>
                </Flex>

        </Flex>
    </div>
}
export default Login;
