import { UserOutlined } from "@ant-design/icons";
import { Button, Card, Flex, Form, Image, Input, Progress, message } from "antd";
import TextField from "../../Components/Input/TextField";
import SecureTextField from "../../Components/Input/SecureTextField";
import {Controller, useForm} from "react-hook-form"
import { useRegister } from "../../ViewModels/Auth/useRegister";
import { LinearProgress } from "@mui/joy";
export const Register = () => {
    const vm = useRegister();
    return <div style={{
        width: "100%",
        height: "100%"
    }}>
        {vm.refresh && <div style={{
            position: "fixed",
            height: 20,
            width: "100%",
            zIndex: 99
        }}>
        <LinearProgress thickness={3} />
        </div>
        }
        <Flex style={{
            height: "100%"
        }}>
            <div style={{
                height: "100%",
                width: window.innerWidth < 1000 ? "0%" : "50%"
            }}>
            <Image
            wrapperStyle={{
                height: "100%"
            }}
            style={{
                height: "100%",
                objectFit: "cover"
            }} srcSet={"https://source.unsplash.com/random/1600x900?Payment"} />
            </div>


                <Flex style={{flexDirection: "column", height: "100%", width: window.innerWidth < 1000 ? "100%" : "50%"}}>
                  <div style={{flex: 1}}></div>
                  <Card>
                   {vm.contextHolder}
                  <Form form={vm.form}
                    onFinish={(val) => vm.validForm(val)}
                    onFinishFailed={(invalid) => vm.invalidForm(invalid)}
                    autoComplete={"off"}
                    layout={"vertical"}>
                    <Form.Item
                        name={"username"}
                        label={"Username"}
                        required={true}
                        rules={[
                            {required: true, message: "Username Tidak Boleh Kosong !"}
                        ]}
                        hasFeedback
                    >

                        <TextField />
                    </Form.Item>
                    <Form.Item
                        required={true}
                        label={"Email"}
                        name={"Email"}
                        rules={[
                            {required: true, message: "Email Tidak Boleh Kosong !"},
                            {type: "email", message: "Format Email Tidak Valid"}
                        ]}
                        hasFeedback
                    >

                        <TextField />
                    </Form.Item>

                    <Form.Item required={true} name={"password"} label={"Password"}
                        rules={[
                            {required: true, message: "Password Tidak Boleh Kosong"},
                            {pattern: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*\W)(?!.* ).{8,16}$/, message: "Format Password Tidak Valid.\n Password Harus Memiliki minimal 1 Angka, 1 Huruf Kecil, 1 Huruf Besar dan 1 Simbol"}
                        ]}
                        dependencies={['password']}

                        hasFeedback
                    >
                        <SecureTextField

                        />
                    </Form.Item>
                    <Form.Item
                    dependencies={['password']}

                    required={true} name={"confirm_pass"} label={"Confirm Password"}
                    rules={[
                        {required: true, message: "Konfirmasi Password Tidak Boleh Kosong"},
                        {pattern: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*\W)(?!.* ).{8,16}$/, message: "Format Password Tidak Valid.\n Password Harus Memiliki minimal 1 Angka, 1 Huruf Kecil, 1 Huruf Besar dan 1 Simbol"},
                        ({ getFieldValue }) => ({
                            validator(_, value) {
                              if (!value || getFieldValue('password') === value) {
                                return Promise.resolve();
                              }
                              return Promise.reject(new Error('Konfirmasi Password Tidak Sama dengan Password'));
                            },
                          }),
                    ]}
                    hasFeedback
                    >
                    <SecureTextField />

                    </Form.Item>

                    <Form.Item>
                        <Button htmlType="submit" type={"primary"} style={{width: "100%"}}>
                            <UserOutlined />
                            Sign Up
                        </Button>
                        <Button onClick={() => {
                            window.location.href = "/login"
                        }} htmlType="button" type={"text"} style={{width: "100%"}}>
                            Already Have An Account ?
                            Sign In Here
                        </Button>
                    </Form.Item>
                  </Form>
                  </Card>
                  <div style={{flex: 1}}></div>
                </Flex>

        </Flex>
    </div>
}
export default Register
