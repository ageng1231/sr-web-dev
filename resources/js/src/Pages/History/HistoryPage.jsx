import {  Button, Col, Descriptions, Divider, Flex, Grid, Image, Modal, Row, Space, Table, Tag, Typography } from "antd"
import MenuDashboard from "../../Components/Menu/MenuDashboard"
import LoggedInLayout from "../../Layouts/LoggedInLayout"

import TextField from "../../Components/Input/TextField"

import { CloseOutlined, EyeOutlined, HistoryOutlined, SearchOutlined, SortAscendingOutlined } from "@ant-design/icons"
import useHistoryPage from "../../ViewModels/HIstory/useHistoryPage"
import Column from "antd/es/table/Column"
import { LinearProgress } from "@mui/joy"
import { StatisticSaldo } from "../../Components/Statistic/StatisticSaldo"
import moment from "moment/moment"
import FilterModal from "../../Components/Modal/FilterModal"
import useFilterModal from "../../ViewModels/Components/useFilterModal"

export const HistoryPage = () => {
    const vm = useHistoryPage();
    const filterModalVm = useFilterModal();
    const onDeleteFilterTag = (type) => {
        switch(type){
            case "status":
                filterModalVm.selectStatusFilter("")
                vm.removeFilterStatus()
            break;
            case "amount":
                filterModalVm.selectByAmount("")
                vm.removeFilterAmount()
            break;
        }
    }
    moment.locale(["id"])
        return <LoggedInLayout>
            <div style={{height: "100%", paddingLeft: 20, paddingTop: 12, paddingRight: 20}}>
                {vm.contextHolder}
                {/* <LinearProgress thickness={2} /> */}
                <MenuDashboard selected={vm.selectedMenu}>
                    <Typography>
                        <Typography.Title>
                            <Flex>
                                <HistoryOutlined />
                                <div style={{width: 20}}></div>
                                Riwayat Transaksi
                            </Flex>
                        </Typography.Title>
                    </Typography>
                    <StatisticSaldo />
                    <Flex style={{marginTop: 30}}>
                        <TextField enterButton={"Search"} addonBefore={<SearchOutlined />} addonAfter={
                                <Button htmlType="button" onClick={() => filterModalVm.openModal()} style={{flex: 1}} type={"text"} size={"small"}>
                                        <SortAscendingOutlined />
                                        Filter
                                </Button>
                        } onChange={({target}) => vm.searchHandler(target.value)} placeholder={"Cari Data Berdasarkan Nomor Transaksi / Keterangan"} />
                        <div style={{width: 10}}></div>


                    </Flex>
                    <div style={{height: 12}}></div>
                    <Flex>
                        {vm.filter.status != "" &&
                            <Tag closable={false} onClose={() => onDeleteFilterTag("status")} color={"blue"} style={{ borderRadius: 12}}>{vm.filter.status}</Tag>
                        }
                        {vm.filter.amount != "" &&
                            <Tag closable={false} onClose={() => onDeleteFilterTag("amount")}  color={"blue"} style={{ borderRadius: 12}}>
                                {vm.filter.amount == "1" &&
                                    "< 1 Juta"
                                }
                                {vm.filter.amount == "2" &&
                                    "1 - 10 Juta"
                                }
                                {vm.filter.amount == "3" &&
                                    "> 10 Juta"
                                }
                            </Tag>
                        }
                    </Flex>
                    <Table
                    pagination={{
                        current: vm.currentPage,
                        defaultPageSize: 10,
                        showSizeChanger: true,
                        pageSizeOptions: [5,10,15,30,50,100, 200,500,1000],
                        onChange:(val) => vm.paginationHandler(val)
                    }}
                    dataSource={vm.dataSource}>
                        {vm.columns.map((i,v) => {
                            if(i.dataIndex == "type_trx"){
                                return <Column responsive={["md"]} title={i.title} dataIndex={i.dataIndex} key={i.key}
                                    render={(_,rec) => {

                                        return <Tag title={rec.type_trx} color={rec.type_trx.includes("outbound") ? "red" : "green"} key={"tag"+rec}>{rec.type_trx.replaceAll("outbound_", "").replaceAll("inbound_", "").toUpperCase()}</Tag>
                                    }}
                                />
                            }
                            return <Column title={i.title} responsive={i.dataIndex == "kode_transaksi" ? ["xs", "md", "sm", "lg", "xl", "xxl"]: ["md"]} dataIndex={i.dataIndex} key={i.key} render={(_,rec) => {

                               if( i.dataIndex == "kode_transaksi"){
                                    return <Button key={"btn" + rec[i.dataIndex]} style={{

                                    }} type={"link"}
                                        onClick={() => {
                                            vm.openModal(rec[i.dataIndex])
                                        }}
                                    >
                                        <p style={{margin: 0, padding: 0}}>
                                        {
                                            rec[i.dataIndex]
                                        }
                                        </p>
                                    </Button>
                               }
                                return<p style={{
                                    color: "grey"
                                }}>
                                    {i.dataIndex == "amount" &&
                                        "IDR " + Intl.NumberFormat().format(rec[i.dataIndex])

                                    }
                                    {i.dataIndex != "amount" &&
                                        rec[i.dataIndex]
                                    }
                                </p>
                            }} />
                        })}

                    </Table>
                </MenuDashboard>
            </div>
            <Modal
            open={vm.modalOpen}
            centered
            width={window.innerWidth > 1000 ? 700 : window.innerWidth}
            onCancel={() => vm.modalClose()}
            footer={[
                <Button key={"close"} onClick={() => vm.modalClose()} type={"primary"}>
                    <CloseOutlined />
                    Close
                </Button>
            ]}
            title={"No. Transaksi : " + vm.modalData.kode_transaksi}>
                <div style={{ height: 20 }}></div>
                <Descriptions column={{xs: 1, sm: 1, md: 1,lg: 1, xl: 2, xxl: 2}}>
                    <Descriptions.Item label={"Tanggal Transaksi" }>

                        {
                            moment(vm.modalData.updated_at).format("LLLL")
                        }
                    </Descriptions.Item>
                    <Descriptions.Item label={"Jenis Transaksi" }>
                        {
                            vm.modalData.type_trx.includes("inbound") &&
                            <Tag color={"green"}>
                                {vm.modalData.type_trx.replace("inbound_", "").toLocaleUpperCase()}
                            </Tag>
                        }
                        {
                            vm.modalData.type_trx.includes("outbound") &&
                            <Tag color={"red"}>
                                {vm.modalData.type_trx.replace("outbound_", "").toLocaleUpperCase()}
                            </Tag>
                        }

                    </Descriptions.Item>
                    <Descriptions.Item label={"Amount Transaksi" }>{vm.modalData.amount}</Descriptions.Item>
                    <Descriptions.Item label={"status Transaksi" }>
                        {
                            vm.modalData.status == "PROCESS" &&
                            <Tag color={"cyan"}>{vm.modalData.status}</Tag>
                        }
                        {
                            vm.modalData.status == "SUCCESS" &&
                            <Tag color={"green"}>{vm.modalData.status}</Tag>
                        }
                         {
                            vm.modalData.status == "CANCELED" &&
                            <Tag color={"red"}>{vm.modalData.status}</Tag>
                        }

                    </Descriptions.Item>
                    <Descriptions.Item label={"Bukti Transaksi" }>
                        <Image
                            style={{
                                width: 150,
                                height: 200,
                                aspectRatio: 3/4,
                                objectFit: "contain"
                            }}
                            srcSet={vm.modalData.img_url}
                        />
                       </Descriptions.Item>

                </Descriptions>
            </Modal>
            <Modal centered open={filterModalVm.modalOpen} onOk={() => {
                vm.changeSelectedFilter(filterModalVm.filterSelected)
                filterModalVm.closeModal()
            }} title={filterModalVm.title} onCancel={() => filterModalVm.closeModal()}>
                <Typography level={5}>
                     Search By Status
                </Typography>
                <div style={{ height: 10 }}></div>
                <Row>
                    <Col>
                        <Button htmlType="button" onClick={() => filterModalVm.selectStatusFilter("PROCESS")} type={filterModalVm.filterSelected.status == "PROCESS" ? "primary" : "text"} >
                            Process
                        </Button>
                        <Button htmlType="button" onClick={() => filterModalVm.selectStatusFilter("SUCCESS")} type={filterModalVm.filterSelected.status == "SUCCESS" ? "primary" : "text"} >
                            Success
                        </Button>
                        <Button htmlType="button" onClick={() => filterModalVm.selectStatusFilter("CANCELED")} type={filterModalVm.filterSelected.status == "CANCELED" ? "primary" : "text"}>
                            Canceled
                        </Button>
                    </Col>
                </Row>
                <div style={{ height: 20 }}></div>
                <Typography level={5}>
                     Search By Nominal
                </Typography>
                <div style={{ height: 10 }}></div>
                <Row>
                    <Col>
                        <Button htmlType="button" onClick={() => filterModalVm.selectByAmount("1")} type={filterModalVm.filterSelected.amount == "1" ? "primary" : "text"} >
                            {"< 1 Juta"}
                        </Button>
                        <Button htmlType="button" onClick={() => filterModalVm.selectByAmount("2")} type={filterModalVm.filterSelected.amount == "2" ? "primary" : "text"} >
                            {"1 - 10 Juta"}
                        </Button>
                        <Button htmlType="button" onClick={() => filterModalVm.selectByAmount("3")} type={filterModalVm.filterSelected.amount == "3" ? "primary" : "text"}>
                            {"> 10 Juta"}
                        </Button>
                    </Col>
                </Row>
            </Modal>
        </LoggedInLayout>
}

export default HistoryPage
