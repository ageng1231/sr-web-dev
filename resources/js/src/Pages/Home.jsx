import { Menu } from "antd";
import React from "react";
import useNavigationBar from "../ViewModels/useNavigationBar";
import NavbarMenu from "../Components/Navbar";
import HomeCarousel from "../Components/Carousel/HomeCarousel";
const { Content } = Layout;
const { Title, Text } = Typography;
import "./../../assets/app.css"
import { Layout, Row, Col, Typography, Button } from "antd";

export const Home = () => {
    const vm = useNavigationBar()
    return <div>
       <NavbarMenu />
       <div style={{
        paddingVertical: 12,
        paddingRight: 20,
        paddingLeft: 20
       }}>
<Content className="home-content">
        {/* Hero Section */}
        <Row justify="center" align="middle" style={{ height: "30vh" }}>
          <Col span={24}>
            <Title level={1}>Welcome to My Technical Test</Title>
            <Text>Technical Test IDE Asia - Ageng Muhammad Wijayanto - Sr. Web Dev.</Text>
            <br></br>
            <Button type="primary">Learn More</Button>
          </Col>
        </Row>

            {/* Benefits Section */}
            <Row gutter={[16, 16]}>
            <Col xs={24} sm={12} md={8}>
                <div className="benefit-item">
                <i className="fas fa-check-circle"></i>
                <p>Benefit 1</p>
                </div>
            </Col>
            <Col xs={24} sm={12} md={8}>
                <div className="benefit-item">
                <i className="fas fa-check-circle"></i>
                <p>Benefit 2</p>
                </div>
            </Col>
            <Col xs={24} sm={12} md={8}>
                <div className="benefit-item">
                <i className="fas fa-check-circle"></i>
                <p>Benefit 3</p>
                </div>
            </Col>
            </Row>

        </Content>
       </div>
    </div>;
}

export default Home;
