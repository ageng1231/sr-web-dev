import { Menu, Row, Col } from "antd";
import useMenuDashboard from "../../ViewModels/Menu/useMenuDashboard";

export const MenuDashboard = (props) => {
    const vm = useMenuDashboard();
    console.warn(window.innerWidth)
    return <div style={{
        width: "100%"
    }}>

    <Row>
        <Col xs={0} md={1}>

        </Col>
        <Col xs={24} md={5}>
                <Menu
                onClick={vm.onClick}
                style={{

                }}
                    selectedKeys={props?.selected ?? "dashboard"}
                    mode={window.innerWidth < 1000 ? "horizontal" :"vertical"}
                    items={vm.listMenu}
                />

        </Col>
        <Col xs={0} md={1}></Col>
        <Col xs={24} md={15}>
        {props.children}
        </Col>
    </Row></div>
}
export default MenuDashboard;
