import { Input } from "antd"

export const TextAreaField = (props) => {

    return <Input.TextArea {...props} />
}
export default TextAreaField
