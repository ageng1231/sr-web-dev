import { PlusSquareTwoTone } from "@ant-design/icons"
import { Button, Col, Row, Statistic } from "antd"
import useSaldo from "../../ViewModels/Components/useSaldo"

export const StatisticSaldo = () => {
    const vm = useSaldo();
    return <Row>

        <Col xs={24} md={6}><Statistic prefix={"IDR"} title={"Saldo Saat Ini"} value={vm.saldo.balance} /></Col>
        <Col xs={24}  md={6}>
            <Statistic prefix={"IDR"} title={"Total Transaksi Bulan Ini"} value={vm.saldo.month_trx} />

        </Col>
        <Col  xs={24} md={6}> <Statistic prefix={"IDR"} title={"Total Transaksi"} value={vm.saldo.total_trx} /></Col>
        <Col xs={24}  md={3}></Col>
        <Col xs={24}  md={3} style={{
            display: "flex",
            alignItems: "center"
        }}>
            <Button onClick={() => {
                window.location.href = "/transaksi/topup"
            }} type={"primary"}>
                <PlusSquareTwoTone />
                Top Up
            </Button>
        </Col>
    </Row>
}
