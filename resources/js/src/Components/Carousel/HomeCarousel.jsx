import { Carousel, Flex, Image, Radio } from "antd";
import useCarousel from "../../ViewModels/Carousels/useCarousels";

export const HomeCarousel = () => {
    const vm = useCarousel();

    return <div style={{
        display: 'flex',
        flexDirection: "row"
    }}>

        <Carousel

        style={{
            borderRadius: 12,

        }} dotPosition={"bottom"} slidesToShow={1} speed={1000} autoplay >
            {
                vm.imageList.map((i,v) => {
                    return <div key={v} style={{

                    }}>
                        <Image key={v} style={{
                            width: "100%",
                            objectFit: "scretch",
                            borderRadius: 10
                        }} srcSet={i.image} />
                    </div>
                })
            }
        </Carousel>

    </div>
}
export default HomeCarousel;
