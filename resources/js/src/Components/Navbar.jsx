import { Button, Divider, Drawer, Flex, Image, Menu } from "antd";
import useNavigationBar from "../ViewModels/useNavigationBar";
import { MenuOutlined, UserOutlined } from "@ant-design/icons";

export const NavbarMenu = () => {
    const vm = useNavigationBar();
    return <Flex align="center">
                <div style={{width: window.innerWidth > 1000 ? 70 : 20}}></div>
                <Image onClick={() => {
                    window.location.href = "/dashboard"
                }} preview={false} style={{
                    padding: 12,
                    width: 120,
                    objectFit: "cover",
                    cursor: "pointer"
                }} srcSet={"https://yukk.co.id/images/YUKK.png"} />
                <div style={{width: 70}}></div>
                {window.innerWidth > 1000 && <Menu style={{flex: 1}} mode={"horizontal"} items={vm.menu} />
                }
                {
                    vm.isLoggedIn && window.innerWidth > 1000 &&

                    <Menu style={{minWidth: 120}} mode={"horizontal"} items={vm.rightMenu} />
                }
                {
                    !vm.isLoggedIn && window.innerWidth > 1000 &&
                    <Button onClick={() => {
                        window.location.href = "/register"
                    }} type={"primary"}>
                        <UserOutlined />
                        Sign Up
                    </Button>
                }
                {
                    window.innerWidth < 1000 &&
                    <div style={{ flex: 1 }}></div>
                }
                {
                    window.innerWidth < 1000 &&
                    <Button htmlType="button" onClick={() => vm.toggleDrawer()}>
                        <MenuOutlined />
                    </Button>
                }
                <div style={{width: window.innerWidth > 1000 ? 70 : 20}}></div>

                <Drawer open={vm.openDrawer} onClose={() => vm.toggleDrawer()} placement="left">
                    <Menu style={{flex: 1}} mode={"inline"} items={vm.menu} />
                    <Divider />
                    {vm.isLoggedIn && <Menu style={{flex: 1}} mode={"inline"} items={vm.rightMenu} /> }
                    {!vm.isLoggedIn && <Button style={{width: "100%"}} onClick={() => {
                        window.location.href = "/register"
                    }} type={"primary"}>
                        <UserOutlined />
                        Sign In
                    </Button>
                    }
                </Drawer>
            </Flex>
}
export default NavbarMenu;
