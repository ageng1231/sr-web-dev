import { Modal } from "antd";
import useFilterModal from "../../ViewModels/Components/useFilterModal"

export const FilterModal = (props) => {
    const vm = useFilterModal();
    return <div>
        <Modal open={vm.modalOpen} onCancel={() => vm.closeModal()}>
            {props.children}
        </Modal>
    </div>
}
export default FilterModal
