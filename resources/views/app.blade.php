<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Fetch project name dynamically -->
    <title inertia>{{ config('app.name', 'Laravel') }}</title>
    <style>
        html, body{
            height: 100%;
        }
        #app{
            height: 100% !important;
            display: block;
        }
    </style>
    @viteReactRefresh
    <!-- Scripts -->
    @vite('resources/js/app.jsx') @inertiaHead
  </head>

  <body style="margin: 0" class="font-sans antialiased">
    @inertia
  </body>
</html>
