<?php

use App\Http\Controllers\BE\AuthController;
use App\Http\Controllers\BE\BETransasksiController;
use App\Http\Controllers\BE\FileController;
use App\Http\Controllers\FE\Auth\LoginController;
use App\Http\Controllers\FE\Auth\RegisterController;
use App\Http\Controllers\FE\DashboardController;
use App\Http\Controllers\FE\HistoryController;
use App\Http\Controllers\FE\HomeController;
use App\Http\Controllers\FE\TransaksiController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BE\HistoryController as BEHistoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, "show"]);
Route::get("/login", [LoginController::class, "show"]);
Route::get("/register", [RegisterController::class, "show"]);
Route::get("/logout", [AuthController::class, "logout"]);
//Auth Needed
Route::get("/dashboard", [DashboardController::class,"show"]);
Route::get("/history", [HistoryController::class,"show"]);

Route::group(["middleware" => "web", "prefix" => "transaksi"],function () {
    Route::get("/", [TransaksiController::class,"show"]);
    Route::get("/topup", [TransaksiController::class,"topupShow"])->middleware("web");
    Route::get("/transfer", [TransaksiController::class,"transferShow"]);
});
Route::prefix("saldo")->group(function(){
    Route::get("/", [DashboardController::class, "saldo"]);
    Route::post("/transfer", [BETransasksiController::class, "store"]);
});
Route::prefix("file")->group(function(){
    Route::post("upload/bukti_trx", [FileController::class, "uploadTrx"]);
});
Route::prefix("history")->group(function(){
    Route::get("/all", [BEHistoryController::class, "all"]);
    Route::post("/all/q", [BEHistoryController::class, "query"]);
    Route::get("/find/{kodeTransaksi}", [BEHistoryController::class, "find"]);
});
Route::prefix("auth")->group(function(){
    Route::post("signin", [AuthController::class, "login"])->name("login");
});
