<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = "transaction";

    protected $appends = ['img_url'];

    public function getImgUrlAttribute(){
        if($this->status == "PROCESS"){
            return asset("storage/tmp/" . $this->file);
        }else{
            return asset("storage/tmp/" . $this->file);
        }
    }
}
