<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
       Inertia::share("app", [
            "baseUri" => env("APP_URL"),
            "name" => env("APP_NAME"),
            "debug" => env("APP_DEBUG"),
            "env" => env("APP_ENV"),
            "page" => csrf_token(),
            "page_1" => csrf_field()
       ]);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
