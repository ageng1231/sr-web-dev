<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Throwable;
use Log;

class AuthController extends Controller
{
    //
    public function login(Request $request){
        try{
            $user = User::where("email", $request->username)->first();
            if(empty($user)){
                throw new BadRequestException("Email Anda Salah");
            }
            $compare = Hash::check($request->password, $user->password);
            if(!$compare){
                throw new BadRequestException("Password Anda Salah");
            }
            unset($user->password);
            Auth::login($user);
            return response()->json([
                "success" => true,
                "message" => "Berhasil Melakukan Login",
                "data" => $user
            ]);
        }catch(Throwable $e){
            Log::error($e->getMessage(). "\n"."payload :".json_encode($request->toArray())."\n".$e->getTraceAsString());
            return response()->json([
                "success" => false,
                "message" => $e->getMessage(),
                "data" => []
            ], 400);
        }

    }
    public function logout(){
        Auth::logout();
        return redirect("/");
    }
}
