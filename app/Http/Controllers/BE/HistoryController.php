<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use App\Models\Transactions;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class HistoryController extends Controller
{
    //

    public function all(){
        try{
            if(!Auth::guard("web")->check()) throw new UnauthorizedException("You are not allowed to access this page");
            $transaction = Transactions::where("user_id", Auth::guard("web")->user()->id)->orderBy("updated_at", "DESC")->get();
            if(empty($transaction)) throw new BadRequestHttpException("No Transaction Can Be Found");
            return response()->json([
                "success" => true,
                "message" => "Berhasil Mengambil Data Transaksi",
                "data" => $transaction
            ], 201);
        }catch(Exception $e){
            \Log::error($e->getMessage()."\n".$e->getTraceAsString());
            return response()->json([
                "success" => false,
                "message" => $e->getMessage(),
                "data" => []
            ], 400);
        }
    }
    public function query(Request $request){
        try{
            if(!Auth::guard("web")->check()) throw new UnauthorizedException("You are not allowed to access this page");


            $transaction = Transactions::where("user_id", Auth::guard("web")->user()->id)->where(function($q)use($request){
                if(!empty($request->q)){
                    $q->where("kode_transaksi", "like", "%".$request->q."%")->orWhere("keterangan", "like", "%".$request->q."%");

                }
                $filter = (object)$request->filter;

                if(!empty($filter->status)){
                    $q->where("status", $filter->status);
                }
                if(!empty($filter->amount)){
                    switch($filter->amount){
                        case "1":
                            $q->where("amount", "<", (int)"1000000");
                        break;
                        case "2":
                            $q->where("amount", ">", (int)"1000000")->where("amount", "<", (int)"10000000");
                        break;
                        case "3":
                            $q->where("amount", ">", (int)"10000000");
                        break;
                    }
                }
            })->orderBy("updated_at", "DESC")->get();

            if(empty($transaction)) throw new BadRequestHttpException("No Transaction Can Be Found");
            return response()->json([
                "success" => true,
                "message" => "Berhasil Mengambil Data Transaksi",
                "data" => $transaction
            ], 201);
        }catch(Exception $e){
            return response()->json([
                "success" => false,
                "message" => $e->getMessage(),
                "data" => []
            ], 400);
        }
    }
    public function find(String $kodeTransaksi){
        try{
            if(!Auth::guard("web")->check()) throw new UnauthorizedException("You are not allowed to access this page");
            $transaction = Transactions::where("user_id", Auth::guard("web")->user()->id)->where("kode_transaksi", $kodeTransaksi)->first();
            if(empty($transaction)) throw new BadRequestHttpException("No Transaction Can Be Found");
            return response()->json([
                "success" => true,
                "message" => "Berhasil Mengambil Data Transaksi",
                "data" => $transaction
            ], 201);
        }catch(Exception $e){
            return response()->json([
                "success" => false,
                "message" => $e->getMessage(),
                "data" => []
            ], 400);
        }
    }
}
