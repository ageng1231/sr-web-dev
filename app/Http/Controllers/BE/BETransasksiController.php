<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use App\Models\Transactions;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class BETransasksiController extends Controller
{
    //
    public function store(Request $request){
        try{
            if(!Auth::guard("web")->check()) throw new UnauthorizedException("You are not allowed to access this page");
            $transaction = new Transactions;
            $transaction->type_trx = $request->type;
            $transaction->keterangan = $request->keterangan;
            $transaction->amount = $request->amount;
            $transaction->file = $request->bukti_transaksi;
            $transaction->status = "SUCCESS";
            $transaction->kode_transaksi = Str::uuid()->toString();
            $transaction->user_id = Auth::guard("web")->user()->id;
            $save = $transaction->save();
            if(!$save){
                throw new BadRequestException("Gagal Melakukan Input Data Form Transaksi");
            }
            $query = DB::select("CALL HitungSaldo()", []);

            return response()->json([
                "success" => true,
                "message" => "Berhasil Meyimpan Data Transaksi",
                "data" => $transaction
            ], 201);
        }catch(Exception $e){
            return response()->json([
                "success" => false,
                "message" => $e->getMessage(),
                "data" => []
            ], 400);
        }
    }
}
