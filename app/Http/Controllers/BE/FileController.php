<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class FileController extends Controller
{
    //
    public function uploadTrx(Request $request){
        try{
            if(!Auth::guard("web")->check()) throw new UnauthorizedException("You are not allowed to access this page");
            if(empty($request->file("bukti_trx"))) throw new BadRequestException("Tolong Pilih File Terlebih Dahulu");
            $store = $request->file("bukti_trx")->store("public/tmp/bukti_trx");
            $file = $request->file("bukti_trx");

            return response()->json([
                "success" => true,
                "message" => "Berhasil Menyimpan File Bukti Transaksi",
                "data" => (object)[
                    "name" => str_replace("public/tmp/","",$store),
                    "file_name" => $file->getClientOriginalName(),
                    "size" => $file->getSize()
                ]
            ], 200);
        }catch(Exception $e){
            return response()->json([
                "success" => false,
                "message" => $e->getMessage(),
                "data" => []
            ], 400);
        }
    }
}
