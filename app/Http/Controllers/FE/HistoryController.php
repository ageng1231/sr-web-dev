<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

class HistoryController extends Controller
{
    //
    public function show(){
        return Inertia::render("Pages/History/HistoryPage");
    }
}
