<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

class TransaksiController extends Controller
{
    //
    public function show(){
        return Inertia::render("Pages/Transaksi/TopUpPage");
    }
    public function transferShow(){
        return Inertia::render("Pages/Transaksi/Transfer");
    }
    public function topUpShow(){
        return Inertia::render("Pages/Transaksi/TopUpPage");
    }
}
