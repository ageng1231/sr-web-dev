<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use App\Models\Saldos;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\UnauthorizedException;
use Inertia\Inertia;

class DashboardController extends Controller
{
    //
    public function show(){
        return Inertia::render("Pages/Dashboard/DashboardHome");
    }
    public function saldo(){
        try{
            if(!Auth::guard()->check()) throw new UnauthorizedException("You are not allowed to see Saldo");

            $query = DB::select("SELECT SUM(amount) amount FROM transaction WHERE user_id = ? AND status like ? and MONTH(updated_at) = ? GROUP BY user_id", [Auth::guard("web")->user()->id, "success", date("m")]);
            $query2 = DB::select("SELECT SUM(amount) amount FROM transaction WHERE user_id = ? AND status like ? GROUP BY user_id", [Auth::guard("web")->user()->id, "success"]);

            $saldo = Saldos::Where("user_id", Auth::guard()->user()->id)->first();
            return response()->json([
                "success" => true,
                "message" => "Berhasil Mengambil Data Saldo",
                "balance" => $saldo->saldo,
                "month_trx" => $query[0]->amount ?? 0,
                "total_trx" => $query2[0]->amount ?? 0,
            ],202);
        }catch(Exception $e){
            Log::error($e->getMessage()."\n".$e->getTraceAsString());
            return response()->json([
                "success" => false,
                "message" => $e->getMessage(),
                "balance" => 0,
                "month_trx" => 0,
                "total_trx" => 0
            ]);
        }
    }
}
